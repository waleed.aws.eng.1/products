const passport = require('passport');
const { Strategy } = require('passport-local');
const { MongoClient } = require('mongodb');
const debug = require('debug')('app:localStrategy');

module.exports = function localStrategy() {
    passport.use(
        new Strategy(
            {
                usernameField: 'username',
                passwordField: 'password'
            }, (username, password, done) => {
                const uri = 'mongodb+srv://dbUser:********************@globomantix.4cttn.mongodb.net/?retryWrites=true&w=majority';
                const dbName = 'Globomantix';

                (async function validateUser() {
                    let client;
                    try {
                        client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true});
                        console.log("Connecting...");
                        await client.connect();
                        console.log("Connected");

                        const db = client.db(dbName);
                        console.log('Database Initialized');

                        const user = await db.collection('users').findOne({username});
                        if(user && user.password === password) {
                            done(null, user);
                        } else {
                            done(null, false);
                        }
                    } catch(error) {
                        done(error, false);
                    }
                    client.close();
                }())
            }
        )
    );
};