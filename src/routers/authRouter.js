const express = require('express');
const debug = require('debug')('app:authRouter');
const { MongoClient, ObjectID } = require('mongodb');
const passport = require('passport');

const authRouter = express.Router();

authRouter.route('/login')
    .get((req, res) => {
        res.render('login', { user: req.user });
    })
    .post(
        passport.authenticate('local', {
            successRedirect: '/auth/profile',
            failureRedirect: '/',
        })
    );

authRouter.route('/register')
    .get((req, res) => {
        res.render('register', { user: req.user });
    })
    .post((req, res) => {
        console.log(req.body);
        const { username, password, email } = req.body;
        const uri = 'mongodb+srv://dbUser:********************@globomantix.4cttn.mongodb.net/?retryWrites=true&w=majority';
        const dbName = 'Globomantix';
        const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

        async function run() {
            try {
                console.log('Connecting...');
                await client.connect();
                console.log('Connected');

                const database = client.db(dbName);
                console.log('Database Initialized');

                const user = { username, password, email };

                const results = await database.collection('users').insertOne(user);
                console.log('Record Saved');

                req.login(results.ops[0], () => {
                    res.redirect('/auth/profile');
                });

                client.close();
            } catch(error) {
                debug(error);
            }
        }

        run().catch(console.dir);
    });
    
authRouter.route('/profile').get((req, res) => {
        res.json(req.user);
    });
    
module.exports = authRouter;