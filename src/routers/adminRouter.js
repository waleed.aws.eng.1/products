const express = require('express');
const debug = require('debug')('app:adminRouter');
const productJSON = require('../data/products.json');
const { MongoClient } = require('mongodb');

const adminRouter = express.Router();

adminRouter.route('/').get((req, res) => {
    const uri = 'mongodb+srv://dbUser:********************@globomantix.4cttn.mongodb.net/?retryWrites=true&w=majority';
    const dbName = 'Globomantix';
    const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

    async function run() {
        try {
            await client.connect();

            const database = client.db(dbName);
            const productCollection = database.collection("products");

            const result = await productCollection.insertMany(productJSON);
            client.close();
            res.json(result);
        } catch (e) {

        }
    }

    run().catch(console.dir);
});

module.exports = adminRouter;

