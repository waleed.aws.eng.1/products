const express = require('express');
const debug = require('debug')('app:productRouter');

const { MongoClient, ObjectID } = require('mongodb');

const productRouter = express.Router();
productRouter.use((req, res, next) => {
    if(req.user) {
        next();
    } else {
        res.redirect('/auth/login');
    }
});

productRouter.route('/').get((req, res) => {
    const uri = 'mongodb+srv://dbUser:********************@globomantix.4cttn.mongodb.net/?retryWrites=true&w=majority';
    const dbName = 'Globomantix';
    const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

    async function run() {
        try {
            console.log('Connecting...');
            await client.connect();
            console.log('Connected');

            const database = client.db(dbName);
            console.log('Database Initialized');

            const productCollection = database.collection("products");
            console.log('Collection Loaded');

            const products = await productCollection.find().toArray();
            console.log('Products Loaded');

            client.close();

            res.render('products', {products});
        } catch (error) {
            debug(error)
        }
    }

    run().catch(console.dir);
});

productRouter.route('/:id').get((req, res) => {
    const id = req.params.id;
    const uri = 'mongodb+srv://dbUser:********************@globomantix.4cttn.mongodb.net/?retryWrites=true&w=majority';
    const dbName = 'Globomantix';
    const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

    async function run() {
        try {
            console.log('Connecting...');
            await client.connect();
            console.log('Connected');

            const database = client.db(dbName);
            console.log('Database Initialized');

            const productCollection = database.collection("products");
            console.log('Collection Loaded');

            const product = await productCollection.findOne({ _id: new ObjectID(id) });
            client.close();

            res.render('product', {product});
        } catch (e) {

        }
    }

    run().catch(console.dir);
});
module.exports = productRouter;