const express = require('express');
const chalk = require('chalk');
const debug = require('debug')('app');
const path = require('path');
const morgan = require('morgan');
const passport = require('passport');
const cookieParser = require('cookie-parser');
const session = require('express-session');

const PORT = process.env.PORT || 3000;
const app = express();
const adminRouter = require('./src/routers/adminRouter.js');
const productRouter = require('./src/routers/productRouter.js');
const authRouter = require('./src/routers/authRouter.js');

app.use(morgan('tiny'));
app.use(express.static(path.join(__dirname, '/public/')));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(session({secret: 'ITWorx'}));

require('./src/config/passport.js')(app);

app.set('views', './src/views');
app.set('view engine', 'ejs');

app.use('/admin', adminRouter);
app.use('/products', productRouter);
app.use('/auth', authRouter);

app.get('/', (req, res) => {
    res.render('index', { user: req.user });
});

app.listen(PORT, () => {
    debug(`listening to port ${chalk.green(PORT)}...`);
});